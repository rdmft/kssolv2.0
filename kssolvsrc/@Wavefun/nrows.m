function h = nrows(X)
% WAVEFUN/NROWS Number of rows in the wave function class
%    h = NROWS(X) returns the number of rows in the wave function.
%
%    See also Wavefun, ncols.

h = size(X.psi,1);

end
